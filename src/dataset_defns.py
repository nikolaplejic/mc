"""
Dataset-related types & helpers.

This module contains utilities for handling netCDF4 dataset files on the user's
filesystem. It can traverse a directory and extract all relevant dataset files,
creating an "index" which can subsequently be used by other functions to
retrieve the respective data.
"""

import collections
import pathlib
import re

import netCDF4

DatasetType = collections.namedtuple(
    'DatasetType',
    ['dataset_id', 'dataset_name', 'filename_pattern', 'variables_2d',
     'variables_3d']
)

DatasetType.__doc__ = """
    Describes a single dataset.

    Properties:
    - `dataset_id` - a machine-friendly name for the dataset;
    - `dataset_name` - a human-friendly name for the dataset;
    - `filename_pattern` - a regular expression used to extract metadata from
      the filename of each file in the dataset;
    - `variables_2d`: the relevant 2D variables (i.e. dependent on lat/lng);
    - `variables_3d`: the relevant 3D variables (i.e. dependent on lat/lng/lev)

    See the `standard` definition below for an example.
    """

# --- concrete dataset definitions ---

standard = DatasetType(
  dataset_id="standard",
  dataset_name="Standard",
  filename_pattern=r"openmars_my(?P<my_start>[0-9]+)_ls(?P<ls_start>[0-9]+)" +
                    "_my(?P<my_end>[0-9]+)_ls(?P<ls_end>[0-9]+).nc",
  variables_2d=['ps', 'tsurf', 'co2ice', 'dustcol'],
  variables_3d=['temp', 'u', 'v'],
)
"""
The 'standard' OpenMars dataset.

See: https://ordo.open.ac.uk/articles/OpenMARS_MY24-27_standard_database/7352270
"""

water_vapour = DatasetType(
  dataset_id="water_vapour",
  dataset_name="Water Vapour",
  filename_pattern=r"openmars_vap_my(?P<my_start>[0-9]+)" +
                    "_ls(?P<ls_start>[0-9]+)_my(?P<my_end>[0-9]+)" +
                    "_ls(?P<ls_end>[0-9]+).nc",
  variables_2d=['vapcol'],
  variables_3d=[],
)
"""
The OpenMARS water vapour database.

See: https://ordo.open.ac.uk/articles/OpenMARS_water_vapour_database/7315418
"""

ozone_column = DatasetType(
  dataset_id="ozone_column",
  dataset_name="Ozone Column",
  filename_pattern=r"openmars_ozo_my(?P<my_start>[0-9]+)" +
                    "_ls(?P<ls_start>[0-9]+)_my(?P<my_end>[0-9]+)" +
                    "_ls(?P<ls_end>[0-9]+).nc",
  variables_2d=['o3col'],
  variables_3d=[],
)
"""
The OpenMARS ozone column database.

See: https://ordo.open.ac.uk/articles/OpenMARS_ozone_column_database/7315430
"""

all_datasets = [standard, water_vapour, ozone_column]

# --- file indexing helpers ---

FileEntry = collections.namedtuple(
    'FileEntry',
    ['meta', 'variables_2d', 'variables_3d', 'f']
)

FileEntry.__doc__ = """
    Describes a single file in a dataset.

    Properties:
    - `f`: the `PosixPath` object of the file in question;
    - `meta`: the metadata as extracted by the `DatasetType.filename_pattern`
       regular expression;
    - `variables_2d`: the relevant 2D variables (i.e. dependent on lat/lng);
    - `variables_3d`: the relevant 3D variables (i.e. dependent on lat/lng/lev)
    """


def index_files(directory, dataset_types):
    """
    For every given dataset type, find the relevant files in `directory`.

    The 'dataset type' is described by a filename pattern (a regular expression)
    that determines the file naming convention for the dataset. This pattern is
    used to extract "metadata" about the file based on its name; this metadata is
    mostly the temporal start/end parameters of the file.

    The `FileEntry` objects in the index will be sorted by the start date of the
    entry.
    """

    path = pathlib.Path(directory)
    files = [ f for f in path.iterdir() ]

    idx = { t.dataset_id: [] for t in dataset_types }

    for _file in files:
        for dataset_type in dataset_types:
            re_match = re.match(dataset_type.filename_pattern, _file.name)
            if re_match:
                matches = re_match.groupdict()
                file_entry = FileEntry(
                    meta={ k: int(matches[k]) for k in matches.keys() },
                    variables_2d=dataset_type.variables_2d,
                    variables_3d=dataset_type.variables_3d,
                    f=_file,
                )
                idx[dataset_type.dataset_id].append(file_entry)

    # sort by the start date of the entry
    for dataset_type in dataset_types:
        idx[dataset_type.dataset_id].sort(
            key=lambda e: (e.meta['my_start'], e.meta['ls_start'])
        )

    return idx


def variable_info(dataset_idx):
    """
    For a given indexed dataset, returns more detailed information about each
    of the variables in the dataset.
    """
    if len(dataset_idx) == 0:
        return {}

    vars = {"2D": [], "3D": []}

    file = dataset_idx[0]
    cdf_file = netCDF4.Dataset(file.f, 'r')

    for var_2d in file.variables_2d:
        var = {
            "id": var_2d,
            "name": cdf_file.variables[var_2d].getncattr('FIELDNAM'),
            "units": cdf_file.variables[var_2d].getncattr('UNITS'),
        }
        vars["2D"].append(var)

    for var_3d in file.variables_3d:
        var = {
            "id": var_3d,
            "name": cdf_file.variables[var_3d].getncattr('FIELDNAM'),
            "units": cdf_file.variables[var_3d].getncattr('UNITS'),
        }
        vars["3D"].append(var)

    return vars


def time_boundaries(dataset_idx):
    """
    Given an indexed dataset, returns the min/max year and Ls for the dataset.
    """

    min_dt = (dataset_idx[0].meta["my_start"], dataset_idx[0].meta["ls_start"])
    max_dt = (dataset_idx[0].meta["my_end"], dataset_idx[0].meta["ls_end"])

    for f in dataset_idx:
        if f.meta["my_start"] < min_dt[0] or \
           (f.meta["my_start"] == min_dt[0] and f.meta["ls_start"] < min_dt[1]):
            min_dt = (f.meta["my_start"], f.meta["ls_start"])

        if f.meta["my_end"] > max_dt[0] or \
           (f.meta["my_end"] == max_dt[0] and f.meta["ls_end"] > max_dt[1]):
            max_dt = (f.meta["my_end"], f.meta["ls_end"])

    return (min_dt, max_dt)


def files_between(dataset, my_start, ls_start, my_end, ls_end):
    """
    For a given dataset, returns a list of files covering the given time period
    """
    idx_start = 0
    idx_end = 0

    for (idx, f) in enumerate(dataset):
        if f.meta['my_start'] == my_start and f.meta['ls_start'] > ls_start:
            idx_start = idx - 1
            break
        if f.meta['my_end'] > my_start:
            idx_start = idx
            break
        if f.meta['my_start'] < my_start and f.meta['my_end'] == my_end:
            idx_start = idx
            break

    for (idx, f) in enumerate(dataset):
        if f.meta['my_end'] == my_end and f.meta['ls_end'] > ls_end:
            idx_end = idx + 1
            break
        if f.meta['my_end'] > my_end:
            idx_end = idx + 1
            break

    files = dataset[idx_start:idx_end]

    for (idx, f) in enumerate(files):
        if idx == len(files) - 1:
            continue
        if files[idx].meta['ls_end'] != files[idx + 1].meta['ls_start'] and \
           files[idx].meta['ls_end'] != files[idx + 1].meta['ls_start'] - 1:
            raise Exception("Non-contiguous block of files.")

    return files
