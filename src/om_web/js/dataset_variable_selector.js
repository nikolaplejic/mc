export function validate_date(my, ls) {
  const selected = $("#dataset-select option:selected");
  const allowed_my_from = parseInt(selected.data("my-from"), 10);
  const allowed_my_to = parseInt(selected.data("my-to"), 10);
  const allowed_ls_from = parseInt(selected.data("ls-from"), 10);
  const allowed_ls_to = parseInt(selected.data("ls-to"), 10);

  const allowed_from = allowed_my_from + (allowed_ls_from / 1000.0);
  const allowed_to = allowed_my_to + (allowed_ls_to / 1000.0);

  const date = my + (ls / 1000.0);

  return allowed_from <= date && date <= allowed_to;
}

export var VariableSelector = function ($, cfg) {
  var defaultConfig = {
    only_3d: false
  };

  var config = $.extend(defaultConfig, cfg);

  $("#difference").change(e => {
    let checked = $("#difference").is(":checked");

    if (checked) {
      $("#diff-container").removeClass("d-none");
    } else {
      $("#diff-container").addClass("d-none");
    }
  });

  $("#dataset-select").change(e => {
    const item = $(":selected", e.target).first();
    const url = "/variables/" + item.val();

    $.getJSON(url, d => {
      $("#variable-select").empty();

      let og_2d = $("<optgroup>").attr("label", "2D Variables");
      let og_3d = $("<optgroup>").attr("label", "3D Variables");

      d["2D"].forEach(dim => {
        let item = `<option value="${dim.id}" data-type="2d">
          ${dim.name} [${dim.units}]
        </option>`;
        $(og_2d).append(item);
      });

      d["3D"].forEach(dim => {
        let item = `<option value="${dim.id}" data-type="3d">
          ${dim.name} [${dim.units}]
        </option>`;
        $(og_3d).append(item);
      });

      if (!config.only_3d) {
        $("#variable-select").append(og_2d);
      }
      $("#variable-select").append(og_3d);

      $("#variable-select").trigger("change");
    });

    $("#variable-select").change(e => {
      const selected = $("option:selected", e.target);
      const type = selected.data("type");

      if (type == "3d") {
        $("#lev-container").removeClass("d-none");
      } else {
        $("#lev-container").addClass("d-none");
        $("#plot_lev").val("");
      }
    });
  });

  return {
    hasDifference: function() {
      return $("#difference").is(":checked");
    }
  }
};
