from flask import Blueprint, request, abort, send_file, render_template, \
                  current_app

import numpy
import core
import dataset_defns as ddfn


plotter = Blueprint('plotter', __name__, template_folder='templates')


cmaps = [
     ('Perceptually Uniform Sequential', [
        'viridis', 'plasma', 'inferno', 'magma', 'cividis']),
     ('Sequential', [
        'Greys', 'Purples', 'Blues', 'Greens', 'Oranges', 'Reds',
        'YlOrBr', 'YlOrRd', 'OrRd', 'PuRd', 'RdPu', 'BuPu',
        'GnBu', 'PuBu', 'YlGnBu', 'PuBuGn', 'BuGn', 'YlGn']),
     ('Sequential (2)', [
        'binary', 'gist_yarg', 'gist_gray', 'gray', 'bone', 'pink',
        'spring', 'summer', 'autumn', 'winter', 'cool', 'Wistia',
        'hot', 'afmhot', 'gist_heat', 'copper']),
     ('Diverging', [
        'PiYG', 'PRGn', 'BrBG', 'PuOr', 'RdGy', 'RdBu',
        'RdYlBu', 'RdYlGn', 'Spectral', 'coolwarm', 'bwr', 'seismic']),
     ('Cyclic', ['twilight', 'twilight_shifted', 'hsv']),
     ('Qualitative', [
        'Pastel1', 'Pastel2', 'Paired', 'Accent',
        'Dark2', 'Set1', 'Set2', 'Set3',
        'tab10', 'tab20', 'tab20b', 'tab20c']),
     ('Miscellaneous', [
        'flag', 'prism', 'ocean', 'gist_earth', 'terrain', 'gist_stern',
        'gnuplot', 'gnuplot2', 'CMRmap', 'cubehelix', 'brg',
        'gist_rainbow', 'rainbow', 'jet', 'nipy_spectral', 'gist_ncar'])
]
all_cmaps = [cmap for cmap_type in cmaps for cmap in cmap_type[1]]


@plotter.route('/')
@plotter.route('/latlng')
def lat_lng():
    dataset_types = ddfn.all_datasets
    files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                             ddfn.all_datasets)

    time_boundaries = {}
    for dataset_type in dataset_types:
        time_boundaries[dataset_type.dataset_id] = ddfn.time_boundaries(
            files[dataset_type.dataset_id]
        )

    return render_template(
        "plotter.html", params={"dataset_types": dataset_types,
                                "time_boundaries": time_boundaries,
                                "cmaps": cmaps}
    )


@plotter.route('/time')
def time():
    dataset_types = ddfn.all_datasets
    files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                             ddfn.all_datasets)

    time_boundaries = {}
    for dataset_type in dataset_types:
        time_boundaries[dataset_type.dataset_id] = ddfn.time_boundaries(
            files[dataset_type.dataset_id]
        )

    return render_template(
        "time_plotter.html", params={"dataset_types": dataset_types,
                                     "time_boundaries": time_boundaries,
                                     "type": "time_series"}
    )


@plotter.route('/latls')
def lat_ls():
    dataset_types = ddfn.all_datasets
    files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                             ddfn.all_datasets)

    time_boundaries = {}
    for dataset_type in dataset_types:
        time_boundaries[dataset_type.dataset_id] = ddfn.time_boundaries(
            files[dataset_type.dataset_id]
        )

    return render_template(
        "time_plotter.html", params={"dataset_types": dataset_types,
                                     "time_boundaries": time_boundaries,
                                     "type": "lat_ls",
                                     "cmaps": cmaps}
    )


@plotter.route('/latalt')
def lat_alt():
    dataset_types = ddfn.all_datasets
    files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                             ddfn.all_datasets)

    time_boundaries = {}
    for dataset_type in dataset_types:
        time_boundaries[dataset_type.dataset_id] = ddfn.time_boundaries(
            files[dataset_type.dataset_id]
        )

    return render_template(
        "latalt_plotter.html", params={"dataset_types": dataset_types,
                                       "time_boundaries": time_boundaries,
                                       "cmaps": cmaps}
    )


@plotter.route('/satellite')
def satellite():
    dataset_types = ddfn.all_datasets
    files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                             ddfn.all_datasets)

    time_boundaries = {}
    for dataset_type in dataset_types:
        time_boundaries[dataset_type.dataset_id] = ddfn.time_boundaries(
            files[dataset_type.dataset_id]
        )

    return render_template(
        "satellite.html", params={"dataset_types": dataset_types,
                                  "time_boundaries": time_boundaries,
                                  "cmaps": cmaps}
    )


@plotter.route('/plot_lat_lng.png/<string:dataset>/<string:variable>')
def plot_lat_lng(dataset, variable):
    try:
        files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                                 ddfn.all_datasets)
        my = int(request.args.get('my'), 10)
        ls = float(request.args.get('ls'))
        idx = core.time_idx(my, ls, files[dataset])

        cmap = request.args.get('cmap')
        if cmap not in all_cmaps:
            cmap = 'gist_earth'

        lev_str = request.args.get('lev')
        lev = int(lev_str, 10) if lev_str != "" else None

        topo_cdf_file = None
        if request.args.get("topography") == "true":
            topo_cdf_file = current_app.config['OM_SURFACE_PATH']

        fig = core.latlng_plot(idx[0], idx[1], variable,
                               lev_idx=lev, cmap=cmap,
                               topo_cdf_file=topo_cdf_file)

        return send_file(fig, mimetype="image/png")
    except ValueError:
        return abort(404)


@plotter.route('/plot_lat_lng.diff.png/<string:dataset>/<string:variable>')
def plot_lat_lng_diff(dataset, variable):
    try:
        files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                                 ddfn.all_datasets)
        my_1 = int(request.args.get('my_1'), 10)
        ls_1 = float(request.args.get('ls_1'))
        my_2 = int(request.args.get('my_2'), 10)
        ls_2 = float(request.args.get('ls_2'))

        idx_1 = core.time_idx(my_1, ls_1, files[dataset])
        idx_2 = core.time_idx(my_2, ls_2, files[dataset])

        cmap = request.args.get('cmap')
        if cmap not in all_cmaps:
            cmap = 'gist_earth'

        lev_str = request.args.get('lev')
        lev = int(lev_str, 10) if lev_str != "" else None

        topo_cdf_file = None
        if request.args.get("topography") == "true":
            topo_cdf_file = current_app.config['OM_SURFACE_PATH']

        fig = core.latlng_diff_plot(idx_1[0], idx_1[1],
                                    idx_2[0], idx_2[1],
                                    variable, lev_idx=lev,
                                    cmap=cmap, topo_cdf_file=topo_cdf_file)

        return send_file(fig, mimetype="image/png")
    except ValueError:
        return abort(404)


@plotter.route('/plot_time.png/<string:dataset>/<string:variable>')
def plot_time(dataset, variable):
    try:
        files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                                 ddfn.all_datasets)
        my_from = int(request.args.get('my_from'), 10)
        ls_from = float(request.args.get('ls_from'))
        my_to = int(request.args.get('my_to'), 10)
        ls_to = float(request.args.get('ls_to'))
        lat = float(request.args.get('lat'))
        lon = float(request.args.get('lon'))

        lev_str = request.args.get('lev')
        lev = int(lev_str, 10) if lev_str != "" else None

        fig = core.time_evolution_plot(files[dataset], variable,
                                       my_from, ls_from, my_to, ls_to,
                                       lat, lon, lev_idx=lev)

        return send_file(fig, mimetype="image/png")
    except ValueError:
        return abort(404)


@plotter.route('/plot_latls.png/<string:dataset>/<string:variable>')
def plot_latls(dataset, variable):
    try:
        files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                                 ddfn.all_datasets)
        my_from = int(request.args.get('my_from'), 10)
        ls_from = float(request.args.get('ls_from'))
        my_to = int(request.args.get('my_to'), 10)
        ls_to = float(request.args.get('ls_to'))

        lev_str = request.args.get('lev')
        lev = int(lev_str, 10) if lev_str != "" else None

        cmap = request.args.get('cmap')
        if cmap not in all_cmaps:
            cmap = 'gist_earth'

        fig = core.latls_plot(files[dataset], variable,
                              my_from, ls_from, my_to, ls_to,
                              lev_idx=lev, cmap=cmap)

        return send_file(fig, mimetype="image/png")
    except ValueError:
        return abort(404)


@plotter.route('/plot_latls.diff.png/<string:dataset>/<string:variable>')
def plot_latls_diff(dataset, variable):
    try:
        files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                                 ddfn.all_datasets)

        my_from_1 = int(request.args.get('my_from_1'), 10)
        ls_from_1 = float(request.args.get('ls_from_1'))
        my_to_1 = int(request.args.get('my_to_1'), 10)
        ls_to_1 = float(request.args.get('ls_to_1'))

        my_from_2 = int(request.args.get('my_from_2'), 10)
        ls_from_2 = float(request.args.get('ls_from_2'))
        my_to_2 = int(request.args.get('my_to_2'), 10)
        ls_to_2 = float(request.args.get('ls_to_2'))

        lev_str = request.args.get('lev')
        lev = int(lev_str, 10) if lev_str != "" else None

        cmap = request.args.get('cmap')
        if cmap not in all_cmaps:
            cmap = 'gist_earth'

        fig = core.latls_plot_diff(files[dataset], variable,
                                   my_from_1, ls_from_1, my_to_1, ls_to_1,
                                   my_from_2, ls_from_2, my_to_2, ls_to_2,
                                   lev_idx=lev, cmap=cmap)

        return send_file(fig, mimetype="image/png")
    except ValueError as e:
        raise(e)
        return abort(404)


@plotter.route('/plot_lataltps.png/<string:dataset>/<string:variable>')
def plot_lataltps(dataset, variable):
    try:
        files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                                 ddfn.all_datasets)
        my_from = int(request.args.get('my_from'), 10)
        ls_from = float(request.args.get('ls_from'))
        my_to = int(request.args.get('my_to'), 10)
        ls_to = float(request.args.get('ls_to'))

        cmap = request.args.get('cmap')
        if cmap not in all_cmaps:
            cmap = 'gist_earth'

        fig = core.lataltps_plot(files[dataset], variable,
                                 my_from, ls_from, my_to, ls_to,
                                 cmap=cmap)

        return send_file(fig, mimetype="image/png")
    except ValueError:
        return abort(404)


@plotter.route('/plot_lataltps.diff.png/<string:dataset>/<string:variable>')
def plot_lataltps_diff(dataset, variable):
    try:
        files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                                 ddfn.all_datasets)
        my_from_1 = int(request.args.get('my_from_1'), 10)
        ls_from_1 = float(request.args.get('ls_from_1'))
        my_to_1 = int(request.args.get('my_to_1'), 10)
        ls_to_1 = float(request.args.get('ls_to_1'))

        my_from_2 = int(request.args.get('my_from_2'), 10)
        ls_from_2 = float(request.args.get('ls_from_2'))
        my_to_2 = int(request.args.get('my_to_2'), 10)
        ls_to_2 = float(request.args.get('ls_to_2'))

        cmap = request.args.get('cmap')
        if cmap not in all_cmaps:
            cmap = 'gist_earth'

        fig = core.lataltps_plot_diff(files[dataset], variable,
                                      my_from_1, ls_from_1, my_to_1, ls_to_1,
                                      my_from_2, ls_from_2, my_to_2, ls_to_2,
                                      cmap=cmap)

        return send_file(fig, mimetype="image/png")
    except ValueError as e:
        raise e


@plotter.route('/plot_satellite.png/<string:dataset>/<string:variable>')
def plot_satellite(dataset, variable):
    try:
        files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                                 ddfn.all_datasets)
        my = int(request.args.get('my'), 10)
        ls = float(request.args.get('ls'))
        idx = core.time_idx(my, ls, files[dataset])

        cmap = request.args.get('cmap')
        if cmap not in all_cmaps:
            cmap = 'gist_earth'

        lev_str = request.args.get('lev')
        lev = int(lev_str, 10) if lev_str != "" else None

        topo_cdf_file = None
        if request.args.get("topography") == "true":
            topo_cdf_file = current_app.config['OM_SURFACE_PATH']

        fig = core.satellite_plot(idx[0], idx[1], variable,
                                  lev_idx=lev, cmap=cmap,
                                  topo_cdf_file=topo_cdf_file)

        return send_file(fig, mimetype="image/png")
    except ValueError as e:
        raise e
        return abort(404)


@plotter.route('/csv/<string:dataset>/<string:variable>')
def latlng_csv(dataset, variable):
    try:
        files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                                 ddfn.all_datasets)
        my = int(request.args.get('my'), 10)
        ls = float(request.args.get('ls'))

        idx = core.time_idx(my, ls, files[dataset])

        lev_str = request.args.get('lev')
        lev = int(lev_str, 10) if lev_str != "" else None

        data = core.latlng_data(idx[0], idx[1], variable, lev_idx=lev)

        lons = numpy.concatenate([[0], data['lons']])
        csv_data_lats = numpy.concatenate(
            [[[lat] for lat in data["lats"]], data["var"]], 1
        )
        csv_data = numpy.concatenate([[lons], csv_data_lats])

        csv = core.to_csv(csv_data)

        fn = "{}_{}_{}.csv".format(variable, str(my), str(ls))

        return send_file(csv, mimetype="text/csv", as_attachment=True,
                         attachment_filename=fn)
    except ValueError:
        return abort(404)


@plotter.route('/time.csv/<string:dataset>/<string:variable>')
def time_csv(dataset, variable):
    try:
        files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                                 ddfn.all_datasets)
        my_from = int(request.args.get('my_from'), 10)
        ls_from = float(request.args.get('ls_from'))
        my_to = int(request.args.get('my_to'), 10)
        ls_to = float(request.args.get('ls_to'))
        lat = float(request.args.get('lat'))
        lon = float(request.args.get('lon'))

        lev_str = request.args.get('lev')
        lev = int(lev_str, 10) if lev_str != "" else None

        data = core.time_evolution_at_latlng(files[dataset], variable,
                                             my_from, ls_from, my_to, ls_to,
                                             lat, lon, lev_idx=lev)

        csv = core.to_csv(data)

        fn = "{}_{}_{}.csv".format(variable, str(ls_from), str(ls_to))

        return send_file(csv, mimetype="text/csv", as_attachment=True,
                         attachment_filename=fn)
    except ValueError:
        return abort(404)
