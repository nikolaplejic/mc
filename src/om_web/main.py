from flask import Blueprint, jsonify, abort, current_app, render_template

import dataset_defns as ddfn


main = Blueprint('main', __name__, template_folder='templates')


@main.route('/variables/<string:dataset_type>')
def variable_info(dataset_type):
    dataset_types = ddfn.all_datasets
    files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                             dataset_types)

    if len(files[dataset_type]) == 0:
        return abort(404)

    return jsonify(ddfn.variable_info(files[dataset_type]))


@main.route('/diag')
def diag():
    dataset_types = ddfn.all_datasets
    files = ddfn.index_files(current_app.config['OM_RESOURCE_PATH'],
                             ddfn.all_datasets)

    time_boundaries = {}
    for dataset_type in dataset_types:
        time_boundaries[dataset_type.dataset_id] = ddfn.time_boundaries(
            files[dataset_type.dataset_id]
        )

    return render_template(
        "diag.html", params={"dataset_types": dataset_types,
                             "time_boundaries": time_boundaries,
                             "files": files}
    )
