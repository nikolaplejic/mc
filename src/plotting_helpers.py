import netCDF4
import matplotlib.pyplot as plt


def contour_plot(data, plot_title, colorbar_title, xs, ys,
                 xlabel, ylabel, xticks, yticks, levels, cmap,
                 topo_cdf_file=None, semilogy=False):
    fig, ax = plt.subplots()

    if topo_cdf_file:
        g = netCDF4.Dataset(topo_cdf_file, 'r')
        # the weird indexing is to make the topographic data compatible with
        # the public OpenMARS datasets' longitude/latitude data. without the
        # indexing, the `surface.nc` file includes a couple of data points that
        # the public databases do not, and the plot gets "widened".
        altit = g.variables['zMOL'][1:-2, 1:-4]
        lons1 = g.variables['longitude'][1:-4]
        lats1 = g.variables['latitude'][1:-2]

        toplevs = [-5.2, -3.2, -0.2, 4.8, 9.8, 14.8, 19.8]
        ax.contour(lons1, lats1, altit, toplevs, colors='k',
                   linestyles='solid', linewidths=0.2)

    contour_plot = ax.contourf(xs, ys, data, levels=levels, cmap=cmap,
                               extend="max")

    if semilogy:
        ax.semilogy()

    ax.set(title=plot_title)
    ax.set_aspect('equal')

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    if xticks is not None:
        ax.set_xticks(xticks)

    if yticks is not None:
        ax.set_yticks(yticks)

    cbar = fig.colorbar(contour_plot, orientation='horizontal')
    cbar.set_label(colorbar_title)

    return (fig, ax)
