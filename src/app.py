import os

from flask import Flask, send_from_directory, abort
from om_web.plotter import plotter
from om_web.main import main


app = Flask(__name__)
app.config.from_envvar('OM_SETTINGS')

app.register_blueprint(main)
app.register_blueprint(plotter)


@app.route('/dist/<path:tp>/<path:path>')
def serve_static_libraries(tp, path):
    if tp not in ["css", "js", "nc"]:
        return abort(404)

    return send_from_directory(
        os.path.join(os.path.dirname(__file__), '..', 'dist', tp), path
    )


@app.route('/static/<path:tp>/<path:path>')
def serve_static(tp, path):
    if tp not in ["css", "js"]:
        return abort(404)

    return send_from_directory(
        os.path.join(os.path.dirname(__file__), 'om_web', tp), path
    )


if __name__ == "__main__":
    app.run()
