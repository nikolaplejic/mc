import netCDF4
import numpy
import matplotlib
import matplotlib.pyplot as plt
import io
import cartopy.crs as ccrs
import cartopy.util

import dataset_defns as ddfn
import plotting_helpers as h


# disables the Tk backend which causes all sorts of issues when running in a
# web server context...
matplotlib.use('Agg')

# sets the default color map for all generated plots
plt.set_cmap('gist_earth')

# sets the default number of levels for contour plots
DEFAULT_LEVELS = 30


def file_for_date(my, ls, dataset):
    """
    For a given Mars date, returns a list of `FileEntry` objects with the data
    for the date.
    """

    files = []

    for f in dataset:
        if f.meta["my_start"] == f.meta["my_end"] == my \
                and f.meta["ls_start"] <= int(ls) <= f.meta["ls_end"]:
            files.append(f)
        elif f.meta["my_start"] == my and f.meta["my_end"] > my \
                and f.meta["ls_start"] <= int(ls):
            files.append(f)
        elif f.meta["my_start"] < my and f.meta["my_end"] == my \
                and f.meta["ls_end"] >= int(ls):
            files.append(f)

    return files


def time_idx(my, ls, dataset):
    """
    For a time (in Ls) in a given file, returns the `FileEntry` and the array
    index closest to the time in the file.
    """

    files = file_for_date(my, ls, dataset)

    if len(files) == 0:
        raise Exception("Could not find file for this date.")

    def delta_t(file):
        cdf_file = netCDF4.Dataset(file.f, 'r')

        available_ls = cdf_file.variables["Ls"][:]
        deltas_ls = numpy.absolute(available_ls - ls)

        # tuple: (index, value at index, FileEntry)
        return (deltas_ls.argmin(), deltas_ls[deltas_ls.argmin()], file)

    delta = min(map(delta_t, files), key=lambda e: e[1])

    # tuple: (FileEntry, index)
    return (delta[2], delta[0])


def latlng_data(file_entry, time_idx, variable, lev_idx=None):
    """
    Returns a numpy data array for a given 2D variable at a given time index.

    The required time index can be obtained using the `time_idx` function.

    @see time_idx
    """
    if variable not in file_entry.variables_2d and \
       variable not in file_entry.variables_3d:
        raise Exception("Unknown variable.")

    if variable in file_entry.variables_3d and lev_idx is None:
        raise Exception("Need lev_idx for a 3D variable")

    cdf_file = netCDF4.Dataset(file_entry.f, 'r')

    if variable in file_entry.variables_2d:
        data = cdf_file.variables[variable][time_idx]
    else:
        data = cdf_file.variables[variable][time_idx][lev_idx]

    lats = cdf_file.variables['lat'][:]
    lons = cdf_file.variables['lon'][:]

    return {"var": data, "lats": lats, "lons": lons}


def latlng_plot(file_entry, time_idx, variable, format="png",
                lev_idx=None, cmap='gist_earth', topo_cdf_file=None):
    """
    Plots data for a given variable at a given time index.

    The required time index can be obtained using the `time_idx` function.

    Returns a io.BytesIO object with the plot in the format given by the
    `format` parameter (default: PNG).

    @see time_idx
    """
    plot_buffer = io.BytesIO()

    cdf_file = netCDF4.Dataset(file_entry.f, 'r')
    data = latlng_data(file_entry, time_idx, variable, lev_idx)

    lons = cdf_file.variables['lon'][:]
    lats = cdf_file.variables['lat'][:]

    clab = "{} / [{}]".format(
        cdf_file.variables[variable].getncattr('FIELDNAM'),
        cdf_file.variables[variable].getncattr('UNITS')
    )
    title = cdf_file.variables[variable].getncattr('FIELDNAM')

    xticks = numpy.arange(-180, 180, 30)[1:]
    yticks = numpy.arange(-90, 90, 30)[1:]

    fig, ax = h.contour_plot(data["var"], title, clab, lons, lats,
                             "Longitude", "Latitude", xticks, yticks,
                             DEFAULT_LEVELS, cmap,
                             topo_cdf_file=topo_cdf_file)

    fig.savefig(plot_buffer, format=format)
    plt.close(fig)

    plot_buffer.seek(0)
    return plot_buffer


def latlng_diff_plot(file_entry_1, time_idx_1, file_entry_2, time_idx_2,
                     variable, format="png", lev_idx=None, cmap='gist_earth',
                     topo_cdf_file=None):
    """
    Plots the difference between two lat/lng plots.

    @see time_idx
    """
    plot_buffer = io.BytesIO()

    cdf_file_1 = netCDF4.Dataset(file_entry_1.f, 'r')
    data_1 = latlng_data(file_entry_1, time_idx_1, variable, lev_idx)
    data_2 = latlng_data(file_entry_2, time_idx_2, variable, lev_idx)

    data = data_1["var"] - data_2["var"]

    lons = cdf_file_1.variables['lon'][:]
    lats = cdf_file_1.variables['lat'][:]

    clab = "Difference in {} / [{}]".format(
        cdf_file_1.variables[variable].getncattr('FIELDNAM'),
        cdf_file_1.variables[variable].getncattr('UNITS')
    )
    title = cdf_file_1.variables[variable].getncattr('FIELDNAM')

    xticks = numpy.arange(-180, 180, 30)[1:]
    yticks = numpy.arange(-90, 90, 30)[1:]

    fig, ax = h.contour_plot(data, title, clab, lons, lats,
                             "Longitude", "Latitude", xticks, yticks,
                             DEFAULT_LEVELS, cmap,
                             topo_cdf_file=topo_cdf_file)

    fig.savefig(plot_buffer, format=format)
    plt.close(fig)

    plot_buffer.seek(0)
    return plot_buffer


def time_evolution(dataset, variable,
                   my_start, ls_start, my_end, ls_end,
                   lev_idx=None):
    """
    For a given start and end date, and a given latitude and longitude, returns
    the value of a given variable over time.
    """
    files = ddfn.files_between(dataset, my_start, ls_start, my_end, ls_end)

    idx_start = time_idx(my_start, ls_start, files)
    idx_end = time_idx(my_end, ls_end, files)

    first_cdf_f = netCDF4.Dataset(files[0].f, 'r')
    last_cdf_f = netCDF4.Dataset(files[-1].f, 'r')

    def extract_data(f):
        if lev_idx:
            return f[:, lev_idx]
        else:
            return f

    if files[0] == files[-1]:
        """
        When our interval falls into a single file only, extracting data is a
        matter of indexing an array.
        """
        my_data = first_cdf_f.variables['MY'][idx_start[1]:idx_end[1]]
        ls_data = first_cdf_f.variables['Ls'][idx_start[1]:idx_end[1]]
        sol_data = first_cdf_f.variables['time'][idx_start[1]:idx_end[1]]
        var_data = extract_data(
            first_cdf_f.variables[variable][idx_start[1]:idx_end[1]]
        )
    else:
        """
        When we deal with multiple files, we have a situation like this:

         file 1 file 2 file 3 file 4
        [--====|======|======|=====-]
           |                      |
           `- start          end -'

        i.e. we only need to treat the files on the edges specially, while
        being careful to extract only the data we need.
        """
        # first file
        my_data = first_cdf_f.variables['MY'][idx_start[1]:]
        ls_data = first_cdf_f.variables['Ls'][idx_start[1]:]
        sol_data = first_cdf_f.variables['time'][idx_start[1]:]
        var_data = extract_data(first_cdf_f.variables[variable][idx_start[1]:])

        for file in files[1:-1]:
            cdf_f = netCDF4.Dataset(file.f, 'r')
            my_data = numpy.append(my_data, cdf_f.variables['MY'][:])
            ls_data = numpy.append(ls_data, cdf_f.variables['Ls'][:])
            sol_data = numpy.append(sol_data, cdf_f.variables['time'][:])
            var_data = numpy.append(var_data,
                                    extract_data(cdf_f.variables[variable][:]),
                                    axis=0)

        # last file
        my_data = numpy.append(
            my_data, last_cdf_f.variables['MY'][:idx_end[1]]
        )
        ls_data = numpy.append(
            ls_data, last_cdf_f.variables['Ls'][:idx_end[1]]
        )
        sol_data = numpy.append(
            sol_data, last_cdf_f.variables['time'][:idx_end[1]]
        )
        var_data = numpy.append(var_data,
                                extract_data(
                                    last_cdf_f.variables[variable][:idx_end[1]]
                                ),
                                axis=0)

    return {
        "my": my_data,
        "ls": ls_data,
        "sol": sol_data,
        "var": var_data,
        "lat": first_cdf_f.variables['lat'][:],
        "lon": first_cdf_f.variables['lon'][:],
    }


def time_evolution_at_latlng(dataset, variable,
                             my_start, ls_start, my_end, ls_end,
                             lat, lon, lev_idx=None):
    data = time_evolution(dataset, variable,
                          my_start, ls_start, my_end, ls_end,
                          lev_idx=lev_idx)

    def min_idx(arr, val):
        deltas = numpy.absolute(arr - val)
        # tuple: (index, value at index)
        return (deltas.argmin(), deltas[deltas.argmin()])

    idx_lat = min_idx(data['lat'], lat)[0]
    idx_lon = min_idx(data['lon'], lon)[0]

    loc_data = numpy.array([p[idx_lat][idx_lon] for p in data['var']])
    return (data['my'], data['ls'], loc_data)


def time_evolution_plot(dataset, variable,
                        my_start, ls_start, my_end, ls_end,
                        lat, lon, format='png', lev_idx=None):
    """
    Creates a time-series plot for a given variable across a given date range.

    Returns a io.BytesIO object with the plot in the format given by the
    `format` parameter (default: PNG).
    """
    data = time_evolution(dataset, variable,
                          my_start, ls_start, my_end, ls_end,
                          lev_idx=lev_idx)

    def min_idx(arr, val):
        deltas = numpy.absolute(arr - val)
        # tuple: (index, value at index)
        return (deltas.argmin(), deltas[deltas.argmin()])

    idx_lat = min_idx(data['lat'], lat)[0]
    idx_lon = min_idx(data['lon'], lon)[0]

    loc_data = numpy.array([p[idx_lat][idx_lon] for p in data['var']])

    plot_buffer = io.BytesIO()

    cdf_file = netCDF4.Dataset(dataset[0].f, 'r')

    clab = "{} [{}]".format(
        cdf_file.variables[variable].getncattr('FIELDNAM'),
        cdf_file.variables[variable].getncattr('UNITS')
    )

    fig, ax = plt.subplots()

    ax.plot(data['sol'], loc_data)
    n = int(len(data['ls'])/5)
    ax.set_xticks(data['sol'][::n])
    ax.set_xticklabels([d for d in data['ls'][::n]])
    ax.set(title=clab)

    ax.set_xlabel("Time [Ls]")
    ax.set_ylabel(clab)

    # add the information on the Mars year for every tick on the plot
    my_ax = ax.twiny()
    my_ax.set_xticks(data['sol'][::n])
    my_ax.set_xticklabels(data['my'][::n])
    my_ax.set_xlabel("Mars Year")
    my_ax.set_xlim(ax.get_xlim())
    my_ax.spines['bottom'].set_position(('outward', 36))
    my_ax.xaxis.set_ticks_position("bottom")
    my_ax.xaxis.set_label_position("bottom")

    fig.savefig(plot_buffer, format=format, bbox_inches='tight')
    plt.close(fig)

    plot_buffer.seek(0)
    return plot_buffer


def latls_plot(dataset, variable, my_start, ls_start, my_end, ls_end,
               format='png', lev_idx=None, cmap='gist_earth'):
    """
    Creates a lat/Ls plot for a given variable across a given date range.

    Returns a io.BytesIO object with the plot in the format given by the
    `format` parameter (default: PNG).
    """
    data = time_evolution(dataset, variable,
                          my_start, ls_start, my_end, ls_end,
                          lev_idx=lev_idx)

    cdf_file = netCDF4.Dataset(dataset[0].f, 'r')

    clab = "{} [{}]".format(
        cdf_file.variables[variable].getncattr('FIELDNAM'),
        cdf_file.variables[variable].getncattr('UNITS')
    )

    plot_buffer = io.BytesIO()

    z_mtot_assim = data['var'].sum(axis=2) / len(data['lon'])
    tranz_co = z_mtot_assim.transpose()

    fig, ax = plt.subplots(figsize=(6, 6))

    contour_plot = ax.contourf(data['sol'], data['lat'], tranz_co,
                               levels=DEFAULT_LEVELS,
                               extend='max', cmap=cmap)
    n = int(len(data['ls'])/6)
    ax.set_xticks(data['sol'][::n])
    ax.set_xticklabels([d for d in data['ls'][::n]])
    ax.set(title=clab)

    yticks = numpy.arange(-90, 90, 30)[1:]
    ax.set_yticks(yticks)

    ax.set_xlabel("Time [Ls]")
    ax.set_ylabel("Latitude")

    # add the information on the Mars year for every tick on the plot
    my_ax = ax.twiny()
    my_ax.set_xticks(data['sol'][::n])
    my_ax.set_xticklabels(data['my'][::n])
    my_ax.set_xlabel("Mars Year")
    my_ax.set_xlim(ax.get_xlim())
    my_ax.spines['bottom'].set_position(('outward', 36))
    my_ax.xaxis.set_ticks_position("bottom")
    my_ax.xaxis.set_label_position("bottom")

    cbar = fig.colorbar(contour_plot, orientation='horizontal', pad=0.25)
    cbar.set_label(clab)

    fig.savefig(plot_buffer, format=format, bbox_inches='tight')
    plt.close(fig)

    plot_buffer.seek(0)
    return plot_buffer


def latls_plot_diff(dataset, variable,
                    my_start_1, ls_start_1, my_end_1, ls_end_1,
                    my_start_2, ls_start_2, my_end_2, ls_end_2,
                    format='png', lev_idx=None, cmap='gist_earth'):
    """
    Creates a lat/Ls difference plot for a given variable across given date
    ranges. The date ranges have to be commensurate, i.e. both date ranges have
    to return matrices of data with equivalent dimensions.

    Returns a io.BytesIO object with the plot in the format given by the
    `format` parameter (default: PNG).
    """
    data_1 = time_evolution(dataset, variable,
                            my_start_1, ls_start_1, my_end_1, ls_end_1,
                            lev_idx=lev_idx)
    data_2 = time_evolution(dataset, variable,
                            my_start_2, ls_start_2, my_end_2, ls_end_2,
                            lev_idx=lev_idx)

    cdf_file = netCDF4.Dataset(dataset[0].f, 'r')

    clab = "{} [{}]".format(
        cdf_file.variables[variable].getncattr('FIELDNAM'),
        cdf_file.variables[variable].getncattr('UNITS')
    )

    plot_buffer = io.BytesIO()

    z_mtot_assim_1 = data_1['var'].sum(axis=2) / len(data_1['lon'])
    tranz_co_1 = z_mtot_assim_1.transpose()
    z_mtot_assim_2 = data_2['var'].sum(axis=2) / len(data_2['lon'])
    tranz_co_2 = z_mtot_assim_2.transpose()

    tranz_co = tranz_co_2 - tranz_co_1

    fig, ax = plt.subplots(figsize=(6, 6))

    contour_plot = ax.contourf(data_1['sol'], data_1['lat'], tranz_co,
                               levels=DEFAULT_LEVELS,
                               extend='max', cmap=cmap)
    n = int(len(data_1['ls'])/6)
    ax.set_xticks(data_1['sol'][::n])
    ax.set_xticklabels([d for d in data_1['ls'][::n]])
    ax.set(title=clab)

    yticks = numpy.arange(-90, 90, 30)[1:]
    ax.set_yticks(yticks)

    ax.set_xlabel("Time [Ls]")
    ax.set_ylabel("Latitude")

    # add the information on the Mars year for every tick on the plot
    my_ax = ax.twiny()
    my_ax.set_xticks(data_1['sol'][::n])
    my_ax.set_xticklabels(data_1['my'][::n])
    my_ax.set_xlabel("Mars Year")
    my_ax.set_xlim(ax.get_xlim())
    my_ax.spines['bottom'].set_position(('outward', 36))
    my_ax.xaxis.set_ticks_position("bottom")
    my_ax.xaxis.set_label_position("bottom")

    cbar = fig.colorbar(contour_plot, orientation='horizontal', pad=0.25)
    cbar.set_label(clab)

    fig.savefig(plot_buffer, format=format, bbox_inches='tight')
    plt.close(fig)

    plot_buffer.seek(0)
    return plot_buffer


def lataltps_plot(dataset, variable, my_start, ls_start, my_end, ls_end,
                  format='png', cmap='gist_earth'):
    """
    Creates a Latitude-Altitude/Pressure plot for a given variable and a given
    time range.

    Returns a io.BytesIO object with the plot in the format given by the
    `format` parameter (default: PNG).
    """
    if variable not in dataset[0].variables_3d:
        raise ValueError("Not a 3D variable.")

    data = time_evolution(dataset, variable,
                          my_start, ls_start, my_end, ls_end,
                          lev_idx=None)
    ps = time_evolution(dataset, 'ps', my_start, ls_start, my_end, ls_end)

    cdf_file = netCDF4.Dataset(dataset[0].f, 'r')

    clab = "{} [{}]".format(
        cdf_file.variables[variable].getncattr('FIELDNAM'),
        cdf_file.variables[variable].getncattr('UNITS')
    )

    sigma = cdf_file.variables['lev'][:]

    z_var = data['var'].sum(axis=3) / len(data['lon'])
    z_ps = ps['var'].sum(axis=2) / len(data['lon'])

    # Average over the period
    zt_var = z_var.sum(axis=0) / float(len(data['ls']))

    # Creates average pressure at each latitude over the time period
    avg_ps = z_ps.sum(axis=0) / float(len(data['ls']))
    # Array of 1's length of sigma levels
    unit = numpy.ones(len(sigma))

    # Produce (lat,lev) array, where flip_lat[i,:] contains latitude i for all
    # elements in that row
    flip_lat = numpy.outer(data['lat'], unit)
    # Produce (lat,lev) array where flip_alt[i,:] has a standard pressure
    # profile for that lat over the levels
    flip_alt = numpy.outer(avg_ps, sigma)
    # Transpose the arrays (to [lev,lat] shape), to use in plot where x,y are
    # now 2-D maps. In effect shifts the columns to match the actual average
    # pressures at each latitude. The pattern on the surface matches the top of
    # the atmosphere!
    interp_lat = numpy.transpose(flip_lat)
    interp_alt = numpy.transpose(flip_alt)

    plot_buffer = io.BytesIO()

    fig, ax = h.contour_plot(zt_var, clab, clab, interp_lat, interp_alt,
                             "Latitude", "Pressure [Pa]",
                             numpy.arange(-60, 90, 30), None,
                             DEFAULT_LEVELS, cmap, semilogy=True)

    fig.savefig(plot_buffer, format=format, bbox_inches='tight')
    plt.close(fig)

    plot_buffer.seek(0)
    return plot_buffer


def lataltps_plot_diff(dataset, variable,
                       my_start_1, ls_start_1, my_end_1, ls_end_1,
                       my_start_2, ls_start_2, my_end_2, ls_end_2,
                       format='png', cmap='gist_earth'):
    """
    Creates a Latitude-Altitude/Pressure difference plot for a given variable,
    between given time ranges.

    Returns a io.BytesIO object with the plot in the format given by the
    `format` parameter (default: PNG).
    """
    if variable not in dataset[0].variables_3d:
        raise ValueError("Not a 3D variable.")

    ps = time_evolution(dataset, 'ps', my_start_1, ls_start_1,
                        my_end_1, ls_end_1)

    data_1 = time_evolution(dataset, variable,
                            my_start_1, ls_start_1, my_end_1, ls_end_1,
                            lev_idx=None)

    data_2 = time_evolution(dataset, variable,
                            my_start_2, ls_start_2, my_end_2, ls_end_2,
                            lev_idx=None)

    cdf_file = netCDF4.Dataset(dataset[0].f, 'r')

    clab = "{} [{}]".format(
        cdf_file.variables[variable].getncattr('FIELDNAM'),
        cdf_file.variables[variable].getncattr('UNITS')
    )

    sigma = cdf_file.variables['lev'][:]

    z_var_1 = data_1['var'].sum(axis=3) / len(data_1['lon'])
    z_ps = ps['var'].sum(axis=2) / len(data_1['lon'])

    z_var_2 = data_2['var'].sum(axis=3) / len(data_2['lon'])

    # Average over the period
    zt_var_1 = z_var_1.sum(axis=0) / float(len(data_1['ls']))
    zt_var_2 = z_var_2.sum(axis=0) / float(len(data_2['ls']))

    # Creates average pressure at each latitude over the time period
    avg_ps = z_ps.sum(axis=0) / float(len(data_1['ls']))
    # Array of 1's length of sigma levels
    unit = numpy.ones(len(sigma))

    # Produce (lat,lev) array, where flip_lat[i,:] contains latitude i for all
    # elements in that row
    flip_lat = numpy.outer(data_1['lat'], unit)
    # Produce (lat,lev) array where flip_alt[i,:] has a standard pressure
    # profile for that lat over the levels
    flip_alt = numpy.outer(avg_ps, sigma)
    # Transpose the arrays (to [lev,lat] shape), to use in plot where x,y are
    # now 2-D maps. In effect shifts the columns to match the actual average
    # pressures at each latitude. The pattern on the surface matches the top of
    # the atmosphere!
    interp_lat = numpy.transpose(flip_lat)
    interp_alt = numpy.transpose(flip_alt)

    zt_var = zt_var_2 - zt_var_1

    plot_buffer = io.BytesIO()

    fig, ax = h.contour_plot(zt_var, clab, clab, interp_lat, interp_alt,
                             "Latitude", "Pressure [Pa]",
                             numpy.arange(-60, 90, 30), None,
                             DEFAULT_LEVELS, cmap, semilogy=True)

    fig.savefig(plot_buffer, format=format, bbox_inches='tight')
    plt.close(fig)

    plot_buffer.seek(0)
    return plot_buffer


def satellite_plot(file_entry, time_idx, variable, format="png",
                   lev_idx=None, cmap='gist_earth', topo_cdf_file=None):
    """
    TBD
    """
    plot_buffer = io.BytesIO()

    cdf_file = netCDF4.Dataset(file_entry.f, 'r')
    data = latlng_data(file_entry, time_idx, variable, lev_idx)

    lons = cdf_file.variables['lon'][:]
    lats = cdf_file.variables['lat'][:]

    clab = "{} / [{}]".format(
        cdf_file.variables[variable].getncattr('FIELDNAM'),
        cdf_file.variables[variable].getncattr('UNITS')
    )

    h = netCDF4.Dataset(topo_cdf_file, 'r')
    alt = h.variables['zMOL'][:]
    lons2 = h.variables['longitude'][:]
    lats2 = h.variables['latitude'][:]

    latsb = numpy.zeros(37)
    latsb[36] = -90.0
    latsb[0:36] = lats
    lats = latsb
    cocolnew_f = numpy.zeros((37, 72))
    cocolnew_f[0:36, :] = data['var'][:, :]
    cocolnew_f[36, :] = (data['var'][35, :].sum()) / len(lons)

    altcyc, lonscyc2 = cartopy.util.add_cyclic_point(alt, lons2)
    cocolcyc_f, lonscyc = cartopy.util.add_cyclic_point(cocolnew_f, lons)
    lons2, lats2 = numpy.meshgrid(lonscyc2, lats2)

    fig = plt.figure()
    ax = plt.axes(projection=ccrs.Orthographic(
        central_latitude=-47.5, central_longitude=125.
    ))
    contour_plot = ax.contourf(lonscyc, lats, cocolcyc_f,
                               levs=12, transform=ccrs.PlateCarree())
    """
    I am thoroughly unsure why this call is needed.

    It seems that some older versions of matplotlib had issues with determining
    the "extent" of contour plots, but we're using a fairly modern version of
    mpl. However, the solutions outlined for old versions in the GitHub
    comments below seem to work. Go figure.

    For future debugging purposes, the only variable that had issues that
    required this call was `ps` -- all others seemed to work fine.

    The `ax.set_global()` call can be replaced with:
        ax.relim()
        ax.autoscale_view()

    https://github.com/SciTools/cartopy/issues/647#issuecomment-169247571
    https://github.com/SciTools/cartopy/issues/811#issuecomment-256202372
    """
    ax.set_global()

    levs2 = [-5.2, -3.2, -0.2, 4.8, 9.8, 14.8, 19.8]
    matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
    ax.contour(lons2, lats2, altcyc, levs2,
               colors='k', transform=ccrs.PlateCarree())

    cbar = fig.colorbar(contour_plot, orientation='horizontal')
    cbar.set_label(clab)

    fig.savefig(plot_buffer, format=format)
    plt.close(fig)

    plot_buffer.seek(0)
    return plot_buffer


def to_csv(data):
    """
    Dumps given data into a CSV file given by `filename`.

    `data` must be a 1D or 2D numpy array.
    """
    if hasattr(data, 'ndim') and data.ndim > 2:
        raise Exception("Can only save 1D or 2D variables to CSV.")

    filename = io.BytesIO()
    numpy.savetxt(filename, data, delimiter=",", fmt="%10.5f")

    filename.seek(0)
    return filename
