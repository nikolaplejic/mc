"""
This is a working(?) polar stereographic plot using Cartopy, translated from
James' Basemap example.

To be deleted after appropriate generic code is written.

TODO: double check that the topography contours are consistent.
"""

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import netCDF4 as nc
import cartopy.crs as ccrs
import cartopy.util

matplotlib.use('TkAgg')

a = nc.Dataset(
    "/home/nikola/devel/mc/_resources/openmars_my24_ls340_my24_ls356.nc", 'r'
)
ticklevs = np.arange(0, 300, 50)

# IMPORTANT! ---------------------
levs = np.arange(0, 300, 50)

lats = a.variables['lat'][:]
lons = a.variables['lon'][:]

cocol_f = a.variables['tsurf'][15, :, :]

h = nc.Dataset('/home/nikola/devel/mc/dist/nc/surface.nc', 'r')
altvar = h.variables['zMOL']
alt = altvar[:]
lons1var = h.variables['longitude']
lons2 = lons1var[:]
lats1var = h.variables['latitude']
lats2 = lats1var[:]

# Create fake polar row as average of row below!
# Doesn't seem to be necessary in the OpenMARS datasets, keeping it for
# completeness' sake.
latsb = np.zeros(37)
latsb[36] = -90.0
latsb[0:36] = lats
lats = latsb
cocolnew_f = np.zeros((37, 72))
cocolnew_f[0:36, :] = cocol_f[:, :]
cocolnew_f[36, :] = (cocol_f[35, :].sum()) / len(lons)

cocol_f = cocolnew_f
# Set levels for contours
levsalt = np.arange(-6., 5., 0.01)
# Wrap around longitude so that it covers the whole globe
# https://scitools.org.uk/cartopy/docs/latest/cartopy/util/util.html#cartopy.util.add_cyclic_point
altcyc, lonscyc2 = cartopy.util.add_cyclic_point(alt, lons2)
cocolcyc_f, lonscyc = cartopy.util.add_cyclic_point(cocol_f, lons)
lons2, lats2 = np.meshgrid(lonscyc2, lats2)

fig = plt.figure()
ax = plt.axes(projection=ccrs.Orthographic(
    central_latitude=-47.5, central_longitude=125.
))
ax.contourf(lonscyc, lats, cocolcyc_f, 12, transform=ccrs.PlateCarree())

levs2 = [-5.2, -3.2, -0.2, 4.8, 9.8, 14.8, 19.8]
matplotlib.rcParams['contour.negative_linestyle'] = 'solid'
CS1 = ax.contour(lons2, lats2, altcyc, levs,
                 colors='k', transform=ccrs.PlateCarree())

fig.show()
