A set of tools for dealing with the [OpenMARS database](https://ordo.open.ac.uk/collections/OpenMARS_database/4278950).

# Installation

Certain libraries used by the project have their own native dependencies.

On recent Debian-based systems, these can be installed as follows:

    apt install libproj-dev libgeos-dev libhdf5-dev

After these have been satisfied:

- make sure you have Python 3.7 and pip installed
- create a virtualenv (usually with `virtualenv --python=python3 .`)
- run `source bin/activate` to enter the virtualenv
- `pip install -r requirements.txt`

# Usage

- make a copy of the `sample.cfg` configuration file (say, to `om.cfg`);
- edit the necessary settings in the configuration file;
- `cd src/` to enter the directory with the application source;
- run `OM_SETTINGS=/path/to/configuration_file.cfg python app.py` to start
  the user interface;
- open the URL listed in the output in the browser.

# Troubleshooting

## Cartopy segfaults

In some circumstances, Cartopy will regularly segfault during any attempts at
its usage. [This issue
comment](https://github.com/SciTools/cartopy/issues/879#issuecomment-298483743)
on GitHub provides a solution that has proved useful:

    pip uninstall shapely; pip install --no-binary :all: shapely

You will want to run this within the virtualenv of the project.

## Issue installing Cartopy with libproj > 5

In newer versions of Debian, the `libproj-dev` dependency packaged in the
official repository is too new for Cartopy. Until Cartopy releases an updated
version which will hopefully work with PROJ 6, there is an unofficial patched
version that can be installed with

    pip install git+https://github.com/snowman2/cartopy.git@5e624fe

You might have to manually

    pip install pykdtree scipy

afterwards.
